﻿using System;
using System.Threading.Tasks;
using BSA21_Lecture3.Common;

namespace BSA21_Lecture3.ConsoleClient
{
    class Program
    {
        static async Task Main()
        {
            Client client = new();

            await client.DemoProjects();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoTasks();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoTeams();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoUsers();
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoTeamsAndUsers();
            await client.DemoTasksByProject(3);
            Console.WriteLine("\n----------------------------------\n");
            await client.DemoProjectsWithTasks();
            Console.WriteLine("\n----------------------------------\n");

            // - 1 -
            await client.DemoTaskCount();
            Console.WriteLine("\n----------------------------------\n");

            // - 2 -
            await client.DemoTasksOnPerformer(22, 100);
            Console.WriteLine("\n----------------------------------\n");

            // - 3 -
            await client.DemoFinishedTask(100, 2021);
            Console.WriteLine("\n----------------------------------\n");

            // - 4 -
            await client.DemoTeamsWithUsers();
            Console.WriteLine("\n----------------------------------\n");

            // - 5 -
            await client.DemoSortedUsers();
            Console.WriteLine("\n----------------------------------\n");

            // - 6 -
            await client.DemoCustomUser(30);

            // - 7 -
            await client.DemoCustomProject(96);
            Console.ReadKey();
        }
    }
}
