﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BSA21_Lecture3.DAL.Entities;
using Newtonsoft.Json;

namespace BSA21_Lecture3.DAL
{
    public static class Requests
    {
        static readonly HttpClient client = new();
        const string host = @"https://bsa21.azurewebsites.net/api/";

        static async Task<string> GetResponseBody(HttpResponseMessage response)
        {
            return await response.Content.ReadAsStringAsync();
        }
        static async Task<string> GetEndPoint(string url)
        {
            var response = await client.GetAsync(url);
            return await GetResponseBody(response);
        }

        public static async Task<IEnumerable<ProjectEntity>> GetProjects()
        {
            var response = await client.GetAsync(host + "projects");
            return JsonConvert.DeserializeObject<IEnumerable<ProjectEntity>>(await GetResponseBody(response));
        }
        public static async Task<IEnumerable<TaskEntity>> GetTasks()
        {
            var response = await client.GetAsync(host + "tasks");
            return JsonConvert.DeserializeObject<IEnumerable<TaskEntity>>(await GetResponseBody(response));
        }
        public static async Task<IEnumerable<TeamEntity>> GetTeams()
        {
            var response = await client.GetAsync(host + "teams");
            return JsonConvert.DeserializeObject<IEnumerable<TeamEntity>>(await GetResponseBody(response));
        }
        public static async Task<IEnumerable<UserEntity>> GetUsers()
        {
            var response = await client.GetAsync(host + "users");
            return JsonConvert.DeserializeObject<IEnumerable<UserEntity>>(await GetResponseBody(response));
        }

        public static async Task<ProjectEntity> GetProject(int id)
        {
            var response = await client.GetAsync(host + $@"projects/{id}");
            return JsonConvert.DeserializeObject<ProjectEntity>(await GetResponseBody(response));
        }
        public static async Task<TaskEntity> GetTask(int id)
        {
            var response = await client.GetAsync(host + $@"tasks/{id}");
            return JsonConvert.DeserializeObject<TaskEntity>(await GetResponseBody(response));
        }
        public static async Task<TeamEntity> GetTeam(int id)
        {
            var response = await client.GetAsync(host + $@"teams/{id}");
            return JsonConvert.DeserializeObject<TeamEntity>(await GetResponseBody(response));
        }
        public static async Task<UserEntity> GetUser(int id)
        {
            var response = await client.GetAsync(host + $@"users/{id}");
            return JsonConvert.DeserializeObject<UserEntity>(await GetResponseBody(response));
        }
    }
}
