﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BSA21_Lecture3.DAL.Entities
{
    [Table("Tasks")]
    public class TaskEntity : BaseEntity
    {
        [JsonProperty("projectId")] public int ProjectId {get; set;}
        [JsonProperty("performerId")] public int PerformerId {get; set;}
        [JsonProperty("name")] public string Name {get; set;}
        [JsonProperty("description")] public string Description {get; set;}
        [JsonProperty("state")] public int State {get; set;}
        [JsonProperty("createdAt")] public DateTime CreatedAt {get; set;}
        [JsonProperty("finishedAt")] public DateTime? FinishedAt {get; set;}
    }
}
