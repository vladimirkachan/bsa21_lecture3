﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BSA21_Lecture3.DAL.Entities
{
    [Table("Projects")]
    public class ProjectEntity : BaseEntity
    {
        [JsonProperty("authorId")] public int AuthorId {get; set;}
        [JsonProperty("teamId")] public int TeamId {get; set;}
        [JsonProperty("name")] public string Name {get; set;}
        [JsonProperty("description")] public string Description {get; set;}
        [JsonProperty("deadline")] public DateTime Deadline {get; set;}
        [JsonProperty("createdAt")] public DateTime CreatedAt { get; set; }
    }
}
