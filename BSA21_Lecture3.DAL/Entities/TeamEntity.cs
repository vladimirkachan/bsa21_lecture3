﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace BSA21_Lecture3.DAL.Entities
{
    [Table("Teams")]
    public class TeamEntity : BaseEntity
    {
        [JsonProperty("name")] public string Name {get; set;}
        [JsonProperty("createdAt")] public DateTime CreatedAt {get; set;}
    }
}
