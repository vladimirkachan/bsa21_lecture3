﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BSA21_Lecture3.DAL.Entities
{
    public abstract class BaseEntity : IEquatable<BaseEntity>
    {
        [JsonProperty("id")] public int Id { get; set; }
         public bool Equals(BaseEntity other)
        {
            return Id == other?.Id;
        }
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
        public override string ToString()
        {
            return Id.ToString();
        }
        public override bool Equals(object obj)
        {
            var other = obj as BaseEntity;
            if (other == null) return false;
            return other.Equals(this);
        }
    }
}
