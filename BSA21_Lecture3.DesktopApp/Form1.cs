﻿using System;
using System.Windows.Forms;
using BSA21_Lecture3.DAL;

namespace BSA21_Lecture3.DesktopApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.DataSource = bindingSource1;
            dataGridView2.DataSource = bindingSource2;
            dataGridView3.DataSource = bindingSource3;
            dataGridView4.DataSource = bindingSource4;
        }

        protected override async void OnLoad(EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            bindingSource1.DataSource = await Requests.GetProjects();
            bindingSource2.DataSource = await Requests.GetTasks();
            bindingSource3.DataSource = await Requests.GetTeams();
            bindingSource4.DataSource = await Requests.GetUsers();
            Cursor = Cursors.Default;
        }
    }
}
