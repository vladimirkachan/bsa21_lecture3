﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.WebAPI
{
    public class AutoMapperConfiguration
    {
        public AutoMapper.MapperConfiguration Get()
        {
            var mapperConfiguration = new AutoMapper.MapperConfiguration(c =>
            {
                c.CreateMap<TeamEntity, TeamDTO>();
                c.CreateMap<TeamDTO, TeamEntity>();

                c.CreateMap<UserEntity, UserDTO>();
                c.CreateMap<UserDTO, UserEntity>();

                c.CreateMap<ProjectEntity, ProjectDTO>();
                c.CreateMap<ProjectDTO, ProjectEntity>();

                c.CreateMap<TaskEntity, TaskDTO>();
                c.CreateMap<TaskDTO, TaskEntity>();
            });
            return mapperConfiguration;
        }

    }
}
