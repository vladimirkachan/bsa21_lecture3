using AutoMapper;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.BLL.Repositories;
using BSA21_Lecture3.BLL.Services;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace BSA21_Lecture3.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSingleton<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IService<TeamEntity, TeamDTO>, TeamService>();
            services.AddScoped<IRepository<TeamEntity>, TeamRepository>();
            services.AddScoped<IService<UserEntity, UserDTO>, UserService>();
            services.AddScoped<IRepository<UserEntity>, UserRepository>();
            services.AddScoped<IService<ProjectEntity, ProjectDTO>, ProjectService>();
            services.AddScoped<IRepository<ProjectEntity>, ProjectRepository>();
            services.AddScoped<IService<TaskEntity, TaskDTO>, TaskService>();
            services.AddScoped<IRepository<TaskEntity>, TaskRepository>();
            var mapper = new AutoMapperConfiguration();
            services.AddSingleton(_ => mapper.Get().CreateMapper());

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "BSA21_Lecture3.WebAPI", Version = "v1" });
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BSA21_Lecture3.WebAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}
