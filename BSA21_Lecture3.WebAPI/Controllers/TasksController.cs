﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL;
using BSA21_Lecture3.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace BSA21_Lecture3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        readonly IService<TaskEntity, TaskDTO> taskService;
        readonly IService<UserEntity, UserDTO> userService;
        readonly IService<ProjectEntity, ProjectDTO> projectService;
        readonly IService<TeamEntity, TeamDTO> teamService;

        public TasksController(IService<TaskEntity, TaskDTO> taskService, 
                               IService<UserEntity, UserDTO> userService, 
                               IService<ProjectEntity, ProjectDTO> projectService,
                               IService<TeamEntity, TeamDTO> teamService)
        {
            this.taskService = taskService;
            this.userService = userService;
            this.projectService = projectService;
            this.teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> Get()
        {
            return Ok(await Task.Run(() => taskService.Get()));
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            var item = await Task.Run(() => taskService.Get(id));
            return item == null ? NotFound($"There is no such task with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<TaskDTO>> Post([FromBody] TaskCreateDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Name)) return BadRequest("The Name field cannot be empty");
            var team = await Task.Run(() => taskService.Create(dto));
            return CreatedAtAction("POST", team);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TaskDTO>> Put(int id, [FromBody] TaskCreateDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Name)) return BadRequest("The Name field cannot be empty");
            dto.Id = id;
            var updated = await Task.Run(() => taskService.Update(dto));
            return updated ? Ok(await Task.Run(() => taskService.Get(id))) : NotFound($"No task with ID {id}");
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await Task.Run(() => taskService.Delete(id));
            return deleted ? NoContent() : NotFound($"No task with ID {id}");
        }

        [HttpGet("performer/{performerId:int}/{maxLength:int?}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTasksOnPerformer(int performerId, int maxLength = 45)
        {
            var query = from task in taskService.Get(t => t.PerformerId == performerId && t.Name.Length < maxLength) select task;
            return Ok(await Task.Run(query.AsEnumerable));
        }

        [HttpGet("performer/{performerId:int}/year/{year:int}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetFinishedTask(int performerId, int year)
        {
            var query = from task in taskService.Get(t => t.PerformerId == performerId && t.FinishedAt != null &&
                                                     t.FinishedAt >= new DateTime(year, 1, 1) &&
                                                     t.FinishedAt <= new DateTime(year, 12, 31)) select task;
            return Ok(await Task.Run(query.AsEnumerable));
        }

        [HttpGet("projectId/{projectId:int}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetTaskByProject(int projectId)
        {
            var query = from task in taskService.Get(t => t.ProjectId == projectId) select task;
            return Ok(await Task.Run(query.AsEnumerable));
        }

    }
}
