﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace BSA21_Lecture3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        readonly IService<UserEntity, UserDTO> userService;
        readonly IService<TaskEntity, TaskDTO> taskService;
        readonly IService<ProjectEntity, ProjectDTO> projectService;
        readonly IService<TeamEntity, TeamDTO> teamService;

        public UsersController(IService<UserEntity, UserDTO> userService, 
                               IService<TaskEntity,TaskDTO> taskService,
                               IService<ProjectEntity, ProjectDTO> projectService,
                               IService<TeamEntity, TeamDTO> teamService)
        {
            this.userService = userService;
            this.taskService = taskService;
            this.projectService = projectService;
            this.teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Get()
        {
            return Ok(await Task.Run(() => userService.Get()));
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            var item = await Task.Run(() => userService.Get(id));
            return item == null ? NotFound($"There is no such user with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] UserCreateDTO dto)
        {
            var value = await Task.Run(() => userService.Create(dto));
            return CreatedAtAction("POST", value);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<UserDTO>> Put(int id, [FromBody] UserCreateDTO dto)
        {
            dto.Id = id;
            var updated = await Task.Run(() => userService.Update(dto));
            return updated ? Ok(await Task.Run(() => userService.Get(id))) : NotFound($"No user with ID {id}");
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await Task.Run(() => userService.Delete(id));
            return deleted ? NoContent() : NotFound($"No user with ID {id}");
        }

        [HttpGet("sortedByNameAndTaskNameLength")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<UserDTO, IEnumerable<TaskDTO>>>>> GetSortedUsers()
        {
            var query = from user in userService.Get()
                        orderby user.FirstName, user.LastName
                        select KeyValuePair.Create(user, 
                                                   from task in taskService.Get(t => t.PerformerId == user.Id)
                                                   orderby task.Name.Length, task.Name.ToUpper()
                                                   select task);
            return Ok(await Task.Run(query.AsEnumerable));
        }

        [HttpGet("custom/{userId:int}")]
        public async Task<ActionResult<CustomUserDTO>> GetUserObject(int userId)
        {
            var user = userService.Get(userId);
            int? teamId = userService.GetEntity(userId).TeamId;
            CustomUserDTO customUser;
            if (teamId == null) customUser = new CustomUserDTO {User = user};
            else
            {
                var projectsQuery = from p in projectService.Get(pe => pe.TeamId == teamId) orderby p.CreatedAt
                                    select p;
                var lastProject = await Task.Run(projectsQuery.LastOrDefault);
                var totalTaskQuery = from t in taskService.Get(te => te.ProjectId == lastProject.Id) select t;
                int totalTaskUnderLastProject = await Task.Run(totalTaskQuery.Count);
                var unperformedTaskQuery = from t in taskService.Get(te => te.PerformerId == userId &&
                                                                     (te.FinishedAt == null ||
                                                                     te.FinishedAt > DateTime.Now)) select t;
                var totalUnperformedTask = await Task.Run(unperformedTaskQuery.Count);
                var longestTaskQuery = from t in taskService.Get(te => te.PerformerId == userId)
                                       orderby (t.FinishedAt ?? DateTime.Now) - t.CreatedAt select t;
                var longestTask = await Task.Run(longestTaskQuery.LastOrDefault);
                customUser = new CustomUserDTO
                {
                    User = user,
                    LastProject = lastProject,
                    TotalTaskUnderLastProject = totalTaskUnderLastProject,
                    TotalUnperformedTask = totalUnperformedTask,
                    LongestTask = longestTask
                };
            }
            return Ok(customUser);
        }

    }
}
