﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace BSA21_Lecture3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        readonly IService<ProjectEntity, ProjectDTO> projectService;
        readonly IService<TaskEntity, TaskDTO> taskService;
        readonly IService<TeamEntity, TeamDTO> teamService;
        readonly IService<UserEntity, UserDTO> userService;

        public ProjectsController(IService<ProjectEntity, ProjectDTO> projectService,
                                  IService<TaskEntity, TaskDTO> taskService,
                                  IService<TeamEntity, TeamDTO> teamService,
                                  IService<UserEntity, UserDTO> userService)
        {
            this.projectService = projectService;
            this.taskService = taskService;
            this.teamService = teamService;
            this.userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectDTO>>> Get()
        {
            return Ok(await Task.Run(() => projectService.Get()));
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            var item = await Task.Run(() => projectService.Get(id));
            return item == null ? NotFound($"There is no such project with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] ProjectCreateDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Name)) return BadRequest("The Name field cannot be empty");
            var value = await Task.Run(() => projectService.Create(dto));
            return CreatedAtAction("POST", value);
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await Task.Run(() => projectService.Delete(id));
            return deleted ? NoContent() : NotFound($"No project with ID {id}");
        }

        [HttpGet("{projectId:int}/taskCount")]
        public async Task<ActionResult<int>> TaskCount(int projectId)
        {
            return await Task.Run((from t in taskService.Get(e => e.ProjectId == projectId) select t).Count);
        }

        [HttpGet("withTasks")]
        public async Task<IEnumerable<KeyValuePair<ProjectDTO, IEnumerable<TaskDTO>>>> GetProjectsWithTasks()
        {
            var query = from p in projectService.Get()
                    select KeyValuePair.Create(p, 
                                               from t in taskService.Get(te => te.ProjectId == p.Id)
                                               select t);
            return await Task.Run(query.AsEnumerable);
        }

        [HttpGet("custom/{projectId:int}")]
        public async Task<ActionResult<CustomProjectDTO>> GetCustomProject(int projectId)
        {
            var customProject = await BuildCustomProject(projectId);
            return Ok(customProject);
        }
        async Task<CustomProjectDTO> BuildCustomProject(int projectId)
        {
            var project = projectService.Get(projectId);
            var tasks = taskService.Get(te => te.ProjectId == projectId);
            var query1 = from t in tasks orderby t.Description.Length select t;
            var query2 = from t in tasks orderby t.Name.Length select t;
            int? teamId = projectService.GetEntity(projectId).TeamId;
            var query3 = from u in userService.Get(ue => ue.TeamId == teamId) select u;
            var customProject = new CustomProjectDTO
            {
                Project = project,
                LongestDescriptionTask = await Task.Run(query1.LastOrDefault),
                ShortestNameTask = await Task.Run(query2.FirstOrDefault),
                TotalCountOfTeamUsers = await Task.Run(query3.Count)
            };
            return customProject;
        }

    }
}
