﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace BSA21_Lecture3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        readonly IService<TeamEntity, TeamDTO> teamService;
        readonly IService<UserEntity, UserDTO> userService;

        public TeamsController(IService<TeamEntity, TeamDTO> teamService, IService<UserEntity, UserDTO> userService)
        {
            this.teamService = teamService;
            this.userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(await Task.Run(() => teamService.Get()));
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            var item = await Task.Run(() => teamService.Get(id));
            return item == null ? NotFound($"There is no such team with Id = {id}") : Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Post([FromBody] TeamCreateDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Name)) return BadRequest("The Name field cannot be empty");
            var team = await Task.Run(() => teamService.Create(dto));
            return CreatedAtAction("POST", team);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TeamDTO>> Put(int id, [FromBody] TeamCreateDTO dto)
        {
            if (string.IsNullOrWhiteSpace(dto.Name)) return BadRequest("The Name field cannot be empty");
            dto.Id = id;
            var updated = await Task.Run(() => teamService.Update(dto));
            return updated ? Ok(await Task.Run(() => teamService.Get(id))) : NotFound($"No team with ID {id}");
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await Task.Run(() => teamService.Delete(id));
            return deleted ? NoContent() : NotFound($"No team with ID {id}");
        }

        [HttpGet("withUsers/{minAge:int}/{maxAge:int}")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<TeamDTO,IEnumerable<UserDTO>>>>> GetTeamsWithUsers(int minAge = 10, int maxAge = 80)
        {
            var query = from team in teamService.Get()
                        orderby team.CreatedAt descending
                        select KeyValuePair.Create(team,
                            from user in userService.Get(u => u.TeamId == team.Id &&
                                                         DateTime.Now.Year - u.BirthDay.Year >= minAge &&
                                                         DateTime.Now.Year - u.BirthDay.Year <= maxAge)
                            orderby user.RegisteredAt descending
                            select user);
            return Ok(await Task.Run(query.AsEnumerable));
        }

        [HttpGet("andUsers")]
        public async Task<ActionResult<IEnumerable<KeyValuePair<TeamDTO, IEnumerable<UserDTO>>>>> GetTeamsWithUsers()
        {
            var query = from t in teamService.Get()
                        select KeyValuePair.Create(t, from u in userService.Get(ue => ue.TeamId == t.Id) select u);
            return Ok(await Task.Run(query.AsEnumerable));
        }
    }
}
