﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA21_Lecture3.Common.DTO
{
    public class CustomProjectDTO
    {
        public ProjectDTO Project {get; set;}
        public TaskDTO LongestDescriptionTask {get; set;}
        public TaskDTO ShortestNameTask {get; set;}
        public int TotalCountOfTeamUsers {get; set;}
    }
}
