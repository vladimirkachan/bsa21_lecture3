﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BSA21_Lecture3.Common.DTO
{
    public class UserCreateDTO : UserDTO
    {
        [JsonIgnore] public override int Id { get => base.Id; set => base.Id = value; }
        [JsonIgnore] public override DateTime RegisteredAt { get => base.RegisteredAt; set => base.RegisteredAt = value; }
    }
}
