﻿using System;
using System.Collections.Generic;

namespace BSA21_Lecture3.Common.DTO
{
    public class ProjectDTO : BaseDTO
    {
        public string Name {get; set;}
        public string Description {get; set;}
        public DateTime Deadline {get; set;}
        public DateTime CreatedAt {get; set;}
    }
}
