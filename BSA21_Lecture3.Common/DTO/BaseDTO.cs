﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA21_Lecture3.Common.DTO
{
    public abstract class BaseDTO : IEquatable<BaseDTO>
    {
        public virtual int Id { get; set; }
        public bool Equals(BaseDTO other)
        {
            return Id == other?.Id;
        }
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
        public override string ToString()
        {
            return Id.ToString();
        }
        public override bool Equals(object obj)
        {
            var other = obj as BaseDTO;
            if (other == null) return false;
            return other.Equals(this);
        }
    }
}
