﻿using System;

namespace BSA21_Lecture3.Common.DTO
{
    public class TeamDTO : BaseDTO
    {
        public string Name {get; set;}
        public virtual DateTime CreatedAt {get; set;} 
    }
}
