﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace BSA21_Lecture3.Common.DTO
{
    public class TaskCreateDTO : TaskDTO
    {
        [JsonIgnore] public override int Id { get => base.Id; set => base.Id = value; }
        [JsonIgnore] public override DateTime CreatedAt { get => base.CreatedAt; set => base.CreatedAt = value; }
        [JsonIgnore] public override int State { get => base.State; set => base.State = value; }
    }
}
