﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA21_Lecture3.Common.DTO
{
    public class CustomUserDTO
    {
        public UserDTO User {get; set;}
        public ProjectDTO LastProject {get; set;}
        public int? TotalTaskUnderLastProject {get; set;}
        public int? TotalUnperformedTask { get; set;}
        public TaskDTO LongestTask {get; set;}
    }
}
