﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Services
{
    public class TaskService : IService<TaskEntity, TaskDTO>
    {
        readonly IRepository<TaskEntity> repository;
        readonly IMapper mapper;

        public TaskService(IRepository<TaskEntity> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public IList<TaskDTO> Get(Expression<Func<TaskEntity, bool>> filter = null)
        {
            return mapper.Map<IList<TaskDTO>>(repository.Get(filter));
        }
        public TaskDTO Get(int id)
        {
            var entity = repository.Get(id);
            return entity == null ? null : mapper.Map<TaskDTO>(entity);
        }
        public TaskDTO Create(TaskDTO dto)
        {
            dto.Id = repository.LastId + 1;
            var entity = mapper.Map<TaskEntity>(dto);
            repository.Create(entity);
            return Get(dto.Id);
        }
        public bool Update(TaskDTO dto)
        {
            return repository.Update(mapper.Map<TaskEntity>(dto));
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
        public TaskEntity GetEntity(int id)
        {
            return repository.Get(id);
        }
    }
}
