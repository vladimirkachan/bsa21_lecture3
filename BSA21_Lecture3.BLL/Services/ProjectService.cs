﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Services
{
    public class ProjectService : IService<ProjectEntity, ProjectDTO>
    {
        readonly IRepository<ProjectEntity> repository;
        readonly IMapper mapper;

        public ProjectService(IRepository<ProjectEntity> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public IList<ProjectDTO> Get(Expression<Func<ProjectEntity, bool>> filter = null)
        {
            return mapper.Map<IList<ProjectDTO>>(repository.Get(filter));
        }
        public ProjectDTO Get(int id)
        {
            var entity = repository.Get(id);
            return entity == null ? null : mapper.Map<ProjectDTO>(entity);
        }
        public ProjectDTO Create(ProjectDTO dto)
        {
            dto.Id = repository.LastId + 1;
            var entity = mapper.Map<ProjectEntity>(dto);
            repository.Create(entity);
            return Get(dto.Id);
        }
        public bool Update(ProjectDTO dto)
        {
            return repository.Update(mapper.Map<ProjectEntity>(dto));
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
        public ProjectEntity GetEntity(int id)
        {
            return repository.Get(id);
        }
    }
}
