﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.BLL.Repositories;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Services
{
    public class TeamService : IService<TeamEntity, TeamDTO>
    {
        readonly IRepository<TeamEntity> repository;
        readonly IMapper mapper;

        public TeamService(IRepository<TeamEntity> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public IList<TeamDTO> Get(Expression<Func<TeamEntity, bool>> filter = null)
        {
            return mapper.Map<IList<TeamDTO>>(repository.Get(filter));
        }
        public TeamDTO Get(int id)
        {
            var entity = repository.Get(id);
            return entity == null ? null : mapper.Map<TeamDTO>(entity);
        }
        public TeamDTO Create(TeamDTO dto)
        {
            dto.Id = repository.LastId + 1;
            dto.CreatedAt = DateTime.Now;
            var entity = mapper.Map<TeamEntity>(dto);
            repository.Create(entity);
            return Get(dto.Id);
        }
        public bool Update(TeamDTO dto)
        {
            return repository.Update(mapper.Map<TeamEntity>(dto));
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
        public TeamEntity GetEntity(int id)
        {
            return repository.Get(id);
        }
    }
}
