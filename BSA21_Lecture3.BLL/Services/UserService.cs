﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.Common.DTO;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Services
{
    public class UserService : IService<UserEntity, UserDTO>
    {
        readonly IRepository<UserEntity> repository;
        readonly IMapper mapper;

        public UserService(IRepository<UserEntity> repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public IList<UserDTO> Get(Expression<Func<UserEntity, bool>> filter = null)
        {
            return mapper.Map<IList<UserDTO>>(repository.Get(filter));
        }
        public UserDTO Get(int id)
        {
            var entity = repository.Get(id);
            return entity == null ? null : mapper.Map<UserDTO>(entity);
        }
        public UserDTO Create(UserDTO dto)
        {
            dto.Id = repository.LastId + 1;
            dto.RegisteredAt = DateTime.Now;
            var entity = mapper.Map<UserEntity>(dto);
            repository.Create(entity);
            return Get(dto.Id);
        }
        public bool Update(UserDTO dto)
        {
            return repository.Update(mapper.Map<UserEntity>(dto));
        }
        public bool Delete(int id)
        {
            return repository.Delete(id);
        }
        public UserEntity GetEntity(int id)
        {
            return repository.Get(id);
        }
    }
}
