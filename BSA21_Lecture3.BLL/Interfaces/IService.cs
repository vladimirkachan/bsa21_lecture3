﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Interfaces
{
    public interface IService<TEntity, TDto>
    {
        IList<TDto> Get(Expression<Func<TEntity, bool>> filter = null);
        TDto Get(int id);
        TDto Create(TDto dto);
        bool Update(TDto dto);
        bool Delete(int id);
        TEntity GetEntity(int id);
    }
}
