﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BSA21_Lecture3.BLL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        int LastId { get; }
        IList<TEntity> Get(Expression<Func<TEntity, bool>> filter = null);
        TEntity Get(int id);
        void Create(TEntity entity);
        bool Update(TEntity entity);
        bool Delete(int id);
    }
}
