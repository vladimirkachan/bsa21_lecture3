﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<ProjectEntity> Projects { get; }
        IRepository<TaskEntity> Tasks { get; }
        IRepository<UserEntity> Users { get; }
        IRepository<TeamEntity> Teams { get; }

    }
}
