﻿using System;
using System.Linq;
using System.Linq.Expressions;
using BSA21_Lecture3.DAL;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Repositories
{
    public sealed class ProjectRepository : BaseRepository<ProjectEntity>
    {
        static ProjectRepository()
        {
            items = Requests.GetProjects().Result.ToList();
        }
        public override int LastId => items.OrderBy(i => i.Id).LastOrDefault()?.Id ?? -1;
        public override ProjectEntity Get(int id)
        {
            return items.FirstOrDefault(i => i.Id == id);
        }
        public override bool Update(ProjectEntity entity)
        {
            var item = Get(entity.Id);
            if (item == null) return false;
            item.AuthorId = entity.AuthorId;
            item.TeamId = entity.TeamId;
            item.Name = entity.Name;
            item.Description = entity.Description;
            item.Deadline = item.Deadline;
            return true;
        }
    }
}
