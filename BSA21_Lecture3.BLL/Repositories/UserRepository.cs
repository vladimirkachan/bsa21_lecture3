﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture3.DAL;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Repositories
{
    public sealed class UserRepository : BaseRepository<UserEntity>
    {
        static UserRepository()
        {
            items = Requests.GetUsers().Result.ToList();
        }
        public override int LastId => items.OrderBy(i => i.Id).LastOrDefault()?.Id ?? -1;
        public override UserEntity Get(int id)
        {
            return items.FirstOrDefault(i => i.Id == id);
        }
        public override bool Update(UserEntity entity)
        {
            var item = Get(entity.Id);
            if (item == null) return false;
            item.TeamId = entity.TeamId;
            item.FirstName = entity.FirstName;
            item.LastName = entity.LastName;
            item.Email = entity.Email;
            item.BirthDay = entity.BirthDay;
            return true;
        }
    }
}
