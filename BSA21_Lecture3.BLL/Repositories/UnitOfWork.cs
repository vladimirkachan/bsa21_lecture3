﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public IRepository<ProjectEntity> Projects {get;} = new ProjectRepository();
        public IRepository<TaskEntity> Tasks {get;} = new TaskRepository();
        public IRepository<UserEntity> Users {get;} = new UserRepository();
        public IRepository<TeamEntity> Teams {get;} = new TeamRepository();
    }
}
