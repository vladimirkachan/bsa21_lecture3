﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BSA21_Lecture3.DAL;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Repositories
{
    public sealed class TeamRepository : BaseRepository<TeamEntity>
    {
        static TeamRepository()
        {
            items = Requests.GetTeams().Result.ToList();
        }
        public override int LastId => items.OrderBy(i => i.Id).LastOrDefault()?.Id ?? -1;
        public override TeamEntity Get(int id)
        {
            return items.FirstOrDefault(i => i.Id == id);
        }
        public override bool Update(TeamEntity entity)
        {
            var item = Get(entity.Id);
            if (item == null) return false;
            item.Name = entity.Name;
            return true;
        }
    }
}
