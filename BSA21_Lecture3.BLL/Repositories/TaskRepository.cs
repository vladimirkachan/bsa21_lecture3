﻿using System.Linq;
using BSA21_Lecture3.DAL;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Repositories
{
    public sealed class TaskRepository : BaseRepository<TaskEntity>
    {
        static TaskRepository()
        {
            items = Requests.GetTasks().Result.ToList();
        }
        public override int LastId => items.OrderBy(i => i.Id).LastOrDefault()?.Id ?? -1;
        public override TaskEntity Get(int id)
        {
            return items.FirstOrDefault(i => i.Id == id);
        }
        public override bool Update(TaskEntity entity)
        {
            var item = Get(entity.Id);
            if (item == null) return false;
            item.ProjectId = entity.ProjectId;
            item.PerformerId = entity.PerformerId;
            item.Name = entity.Name;
            item.Description = entity.Description;
            item.FinishedAt = entity.FinishedAt;
            return true;
        }
    }
}
