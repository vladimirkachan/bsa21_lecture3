﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BSA21_Lecture3.BLL.Interfaces;
using BSA21_Lecture3.DAL.Entities;

namespace BSA21_Lecture3.BLL.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected static IList<TEntity> items;
        public abstract int LastId {get;}
        public IList<TEntity> Get(Expression<Func<TEntity, bool>> filter = null)
        {
            if (filter == null) return items;
            var f = filter.Compile();
            var query = from i in items
                        where f(i)
                        select i;
            return query.ToList();
        }
        public abstract TEntity Get(int id);
        public void Create(TEntity entity)
        {
            items.Add(entity);
        }
        public abstract bool Update(TEntity entity);
        public bool Delete(int id)
        {
            var entity = Get(id);
            return entity != null && items.Remove(entity);
        }
    }
}
